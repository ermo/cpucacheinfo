# cpucacheinfo

The goal of this module is to expose a simple Dlang API for fetching CPU cache information as presented by the Linux® sysfs.

## Use Case:

Help set per-host CPU cache-friendly chunk sizes at runtime for high performance compression/decompression for the SerpentOS moss/boulder packaging tools.

Assumptions:
- Cache sizes stay under 4GiB and can thus be represented by a 32-bit uint.
- The number of cache levels stay under 255 and can thus be represented by an 8-bit ubyte.

## Motivation:

The existing [Dlang core.cpuid module](https://dlang.org/phobos/core_cpuid.html) only works on x86 and Itanium CPUs.

As SerpentOS will be available for ARM, using sysfs seems a good path going forward.

## Code Example:

```dlang
auto cpu = new CpuCacheInfo();
uint l2ChunkSize = cpu.getOptimalChunkSize(CacheLevel.L2);
uint l3ChunkSize = cpu.getOptimalChunkSize(CacheLevel.L3);
```

## Build + run instructions

### rdmd (part of the Digital Mars D compiler tools):

```bash
chmod a+x cpucacheinfo.d
./cpucacheinfo.d
```

### dmd / ldc2

```bash
dmd cpucacheinfo.d
./cpucacheinfo
```

```bash
ldc2 cpucacheinfo.d
./cpucacheinfo
```
