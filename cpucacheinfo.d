#!/usr/bin/env rdmd

/**
 * Copyright © 2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 *
 * The goal of this module is to expose a simple API for fetching CPU cache
 * information as exposed by the Linux® sysfs.
 *
 * Use Case:
 * Help set per-host CPU cache-friendly chunk sizes at runtime for high
 * performance compression/decompression for the SerpentOS moss/boulder
 * packaging tools.
 *
 * Assumptions:
 * - Cache sizes stay under 4GiB and can thus be represented by a 32-bit uint.
 * - The number of cache levels stay under 255 and can thus be represented by an 8-bit ubyte.
 *
 * Motivation:
 * The exising Dlang cpuid module is not guaranteed to work on ARM.
 *
 * As SerpentOS will be available for ARM, using sysfs seems a good path forward.
 *
 * Code Example:
 * auto cpu = new CpuCacheInfo();
 * uint l2ChunkSize = cpu.getOptimalChunkSize(CacheLevel.L2);
 * uint l3ChunkSize = cpu.getOptimalChunkSize(CacheLevel.L3);
 *
 */

import std.conv : to;
import std.datetime;
import std.datetime.stopwatch;
import std.exception;
import std.format;
import std.stdio;
import std.traits;

class CpuCacheInfo
{

public:
    /**
     * Follows sysfs conventions w/L1d corresponding to (...sysfs cpu path...)/cache/index0.
     *
     * Example: CacheLevel.L2
     */
    const enum CacheLevel : ubyte
    {
        L1d = 0,
        L1i = 1,
        L2 = 2,
        L3 = 3,
        L4 = 4,
        L5 = 5
    }

    this()
    {
        foreach (cl; EnumMembers!CacheLevel)
        {
            _cpuCacheLevels[cast(ubyte) cl] = _parseCacheInfo(cl);
        }
    }

    /**
     * Return the chunkSize at which the CPU copies the fastest (sizes selected from CPU cache sizes)
     */
    uint getFastestCacheSize()
    {
        // L1d cache size here
        uint smallestCacheSize = _cpuCacheLevels[0].maxSize;
        uint largestCacheSize = 0;
        uint fastestCacheSize = 0;
        uint[] testCacheSizes = null;  // null is faster to initialise
        Duration smallestTime = 0.seconds;

        foreach(ccl; _cpuCacheLevels)
        {
            if(ccl.isPresent)
            {
                largestCacheSize = ccl.maxSize;
            }
        }

        enforce(smallestCacheSize > 0, "Smallest cache size (%d) is 0?".format(smallestCacheSize));
        enforce(largestCacheSize >= smallestCacheSize,
                "Largest cache size (%d) is smaller than smallest cache size (%d)?".format(largestCacheSize,
                                                                                           smallestCacheSize));

        // sizes we want to test? L1d -> L1d*2 -> L1d*2 until we reach largest size
        fastestCacheSize = smallestCacheSize;
        uint currentCacheSize = smallestCacheSize;
        Duration fastestTime = 60.seconds; // arbitrarily chosen value for "pretty slow"
        while(currentCacheSize <= largestCacheSize)
        {
            //writefln("currentCacheSize: %d\n", currentCacheSize);
            const Duration currentTime = getCopyTime(currentCacheSize);
            if (currentTime < fastestTime)
            {
                fastestTime = currentTime;
                fastestCacheSize = currentCacheSize;
            }
            currentCacheSize = currentCacheSize * 2;
        }
        writefln("fastest time so far: %s (%d bytes)\n", fastestTime, fastestCacheSize);
        return fastestCacheSize;
    }

    /**
     * Measure the time it takes to copy a fixed size blob of data (16 GiB by default)
     * in chunkSize chunks from /dev/zero with a given chunk size in bytes.
     *
     * 16 GiB = 17_179_869_184
     */
    Duration getCopyTime(const(uint) chunkSizeBytes, const(ulong) copyBytes = 17_179_869_184,
                         const(string) inputFile = "/dev/zero", const(uint) iterations = 5)
    {
        enforce(chunkSizeBytes > 0, "chunkSizeBytes of %d makes no sense!".format(chunkSizeBytes));
        enforce(copyBytes > 0, "copyBytes size of %d makes no sense!".format(copyBytes));
        enforce(iterations > 0, "%d iterations makes no sense!".format(iterations));
        enforce(inputFile != "", "Empty inputFile makes no sense!");

        Duration totalTime = 0.seconds;
        File inFile;
        File outFile;

        scope(exit) {
            inFile.close();
            outFile.close();
        }

        try
        {
            inFile = File(inputFile, "r");
            outFile = File("/dev/null", "w");
        }
        catch (StdioException e) {
            writeln("ERROR: ", e);
            return totalTime;
        }

        const ulong chunkCopies = copyBytes / chunkSizeBytes;
        enforce(chunkCopies > 0, "Number of chunkCopies (%d) == 0 makes no sense!".format(chunkCopies));

        writefln("chunkSizeBytes: %d, chunkCopies: %d", chunkSizeBytes, chunkCopies);
        ulong currentCopy = 0;
        auto sw = StopWatch(AutoStart.yes);
        foreach(i; 0 .. iterations)
        {
            //writefln("Iteration %d", i);
            foreach (ubyte[] buffer; chunks(inFile, chunkSizeBytes))
            {
                //outFile.write(buffer);
                if (currentCopy >= chunkCopies) {
                    //writefln("currentCopy: %d, chunkCopies: %d", currentCopy, chunkCopies);
                    break;
                }
                ++currentCopy;
                // writefln("currentCopy: %d (out of %d)", currentCopy, chunkCopies);
            }
            // remember to reset before next iteration!
            currentCopy = 0;
            //writefln("'-- %s", sw.peek());
        }
        sw.stop();
        totalTime = sw.peek();
        writefln("'-- %s\n", totalTime);
        return totalTime;
    }

    /**
     * Returns the largest pow2 value which is smaller than the given CacheLevel getMinSize().
     *
     * Example:
     * An i7 4790k Haswell CPU has a 8MiB L3 cache, in which case 4MiB will be chosen as the optimal chunksize.
     *
     * If a non-uniform cache size hierarchy is found (the big.LITTLE case), return 0.  The caller is
     * expected to check for 0 and decide on their own which chunk size they want to use in that case.
     */
    uint getOptimalChunkSize(const CacheLevel cl) @nogc pure @safe
    {
        uint minCacheSize = getMinSize(cl);
        if (hasUniformSize(cl) && minCacheSize > 1)
        {
            // minCacheSize is assumed to be a multiple of 2
            import std.math : truncPow2;

            return truncPow2(minCacheSize - 1);
        }
        else
        {
            return 0;
        }
    }

    /**
     * Nicely formatted cache info.
     */
    override string toString()
    {
        string cacheInfo;
        foreach (cl; EnumMembers!CacheLevel)
        {
            string lj3cl = _leftJustify(format("%s", cl), 3);
            if (isPresent(cl))
            {
                if (hasUniformSize(cl))
                {
                    cacheInfo ~= format("%s cache size       :\t%12d KiB\n",
                            lj3cl, getMinSize(cl) / 1024);

                    //if (cl != CacheLevel.L1d && cl != CacheLevel.L1i)
                    //{
                    //    cacheInfo ~= format("'-- optimal chunksize:\t%12d bytes\n\n",
                    //            getOptimalChunkSize(cl));
                    //}
                    //else
                    //{
                    //    cacheInfo ~= "\n";
                    //}
                }
                else
                {
                    cacheInfo ~= "Not implemented; Non-uniform " ~ lj3cl
                        ~ " cache sizes detected -- is this a big.LITTLE CPU?\n\n";
                }
            }
            else
            {
                cacheInfo ~= lj3cl ~ " cache not present.\n";
            }
        }
        // cacheInfo ~= "\n\nFastest Cache size: %d bytes".format(getFastestCacheSize());
        return cacheInfo;
    }

    uint getMaxSize(const CacheLevel cl) @nogc pure @safe
    {
        return _cpuCacheLevels[cast(ubyte) cl].maxSize;
    }

    uint getMinSize(const CacheLevel cl) @nogc pure @safe
    {
        return _cpuCacheLevels[cast(ubyte) cl].minSize;
    }

    bool hasUniformSize(const CacheLevel cl) @nogc pure @safe
    {
        return _cpuCacheLevels[cast(ubyte) cl].isUniform;
    }

    bool isPresent(const CacheLevel cl) @nogc pure @safe
    {
        return _cpuCacheLevels[cast(ubyte) cl].isPresent;
    }

private:
    // Parsed cache info read from sysfs per CacheLevel
    struct CpuCacheLevel
    {
        // maxSize can differ from minSize in e.g. big.LITTLE scenarios
        uint maxSize = 0;
        uint minSize = 0;
        bool isPresent = false; // this shall be initialised if the hardware supports this level
        bool isUniform = true; // the default case because maxSize == minSize == 0
    }

    // Save the cache sizes in the corresponding index during initialisation using the enum index (up to CacheLevel.max)
    enum ubyte _maxCacheLevels = CacheLevel.max + 1;
    CpuCacheLevel[_maxCacheLevels] _cpuCacheLevels;

    // default amount of zeros to copy is 16GiB
    static enum _defaultIs16GiB = 16 * 1024 * 1024 * 1024;

    // The only method that actually touches sysfs.
    CpuCacheLevel _parseCacheInfo(const CacheLevel cl, const bool dbg = false)
    {
        import std.algorithm;
        import std.file : dirEntries, exists, readText, SpanMode;
        import std.string;

        // Example sysfs output:
        // $ cat /sys/devices/system/cpu/cpu0/cache/index2/size
        // 2048K
        // $ cat /sys/devices/system/cpu/cpu0/cache/index3/size
        // 8192K

        uint[string] rawCacheInfo;
        CpuCacheLevel ccl;

        // Need to be able translate the cache sizes to bytes and parse 'K' (and possibly 'M') suffixes.
        immutable string[dchar] trTable = ['K': "*1024", 'M': "*1024*1024"];

        // Parse the directory for cpuN subdirectories, otherwise we end up in endless symlink loops
        auto cpuDirs = dirEntries("/sys/devices/system/cpu/", "cpu[0123456789]*", SpanMode.shallow);
        // Need the numerical enum value, not its string representation
        string cacheIndex = format("/cache/index%d/size", cl);
        //writefln(">>> %s cacheIndex: %s", cl, cacheIndex);

        // Need to finagle the sysfs info into a useful set of key:value pairs
        foreach (d; cpuDirs)
        {
            string cacheInfo = d.name ~ cacheIndex;

            // should probably also check that it's readable and that it contains a sane value?
            if (cacheInfo.exists)
            {
                string sysfsCacheSize, trCacheSize;
                uint cacheSize;
                // first get a nicely formatted string
                try
                {
                    sysfsCacheSize = cacheInfo.readText().chomp();
                    // enforce that sysfCacheSize contains numbers and K?
                    // massage into "cacheSize*1024" format instead of "cacheSizeK" to prepare for conversion
                    trCacheSize = translate(sysfsCacheSize, trTable);
                    // enforce that traCacheSize contains numbers and *?
                    cacheSize = _toBytes(trCacheSize);
                }
                catch (Exception ex)
                {
                    writeln(ex);
                    // no useful info was gleaned, so let's pretend the cache is not there?
                    rawCacheInfo[cacheInfo] = 0;
                    break;
                }

                rawCacheInfo[cacheInfo] = cacheSize;

                if (dbg)
                {
                    writefln(">>> %s:\t%12s\t%12s bytes)", cacheInfo,
                            cacheSize, "(" ~ to!string(rawCacheInfo[cacheInfo]));
                }
            }
            else // if the cacheInfo file doesn't exist, it makes sense to set the value to 0
            {
                rawCacheInfo[cacheInfo] = 0;

                if (dbg)
                {
                    writefln(">>> %s:\t%12s\t%12s bytes)", cacheInfo, "N/A",
                            "(" ~ to!string(rawCacheInfo[cacheInfo]));
                }
            }
        }
        // Now that we have cache sizes for all CPUs, let's store the useful information
        ccl.maxSize = rawCacheInfo.values.maxElement;
        ccl.minSize = rawCacheInfo.values.minElement;
        if (ccl.maxSize > 0)
        {
            ccl.isPresent = true;
            if (ccl.maxSize > ccl.minSize)
            {
                ccl.isUniform = false;
            }
        }

        return ccl;
    }

    // left-justify strings
    string _leftJustify(const string s, const ubyte length) pure @safe
    {
        string justified = s; // does this need to be a copy?!
        while (justified.length < length)
            justified ~= " ";
        return justified;
    }

    // Assume that the string has been through a translate call and thus only contains uints and '*' (no spaces!)
    // 8MiB cache -> "8192K" -> "8192*1024" -> 1 * 8192 * 1024 -> 8_388_608 bytes
    uint _toBytes(const string s) pure @safe
    {
        import std.algorithm.searching : findSplit;

        uint result = 1; // multiplicative identity
        auto byteArray = findSplit(s, "*");
        foreach (val; byteArray)
        {
            if (val == "*")
            {
                continue;
            }
            result *= to!uint(val);
        }
        return result;
    }
    // CpuCacheInfo end
}

void main()
{
    auto cpu = new CpuCacheInfo();
    writeln(cpu);
    writefln("Fastest cache size: %d", cpu.getFastestCacheSize());
}
